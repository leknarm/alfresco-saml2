# README #

Filter alfresco authentication to SAML

### Configure web.xml ###

* Add SAMLAuthenticationFilter to filter section before share SSO

```
   <filter>
      <description>SAML Authentication Filter</description>
      <filter-name>SAMLAuthenticationFilter</filter-name>
      <filter-class>com.leknarm.saml.sp.SAMLAuthenticationFilter</filter-class>
   </filter>
```
* Add filter mapping to filter mapping section before Authentication
```
   <filter-mapping>
      <filter-name>SAMLAuthenticationFilter</filter-name>
      <url-pattern>/page/*</url-pattern>
   </filter-mapping>

   <filter-mapping>
      <filter-name>SAMLAuthenticationFilter</filter-name>
      <url-pattern>/p/*</url-pattern>
   </filter-mapping>
```
* Add servlet and servlet mapping to servlet section
```
   <servlet>
      <servlet-name>ACSServlet</servlet-name>
      <servlet-class>com.leknarm.saml.sp.ACSServlet</servlet-class>
   </servlet>
```
```
   <servlet-mapping>
      <servlet-name>ACSServlet</servlet-name>
      <url-pattern>/acs</url-pattern>
   </servlet-mapping>
```
### Configure share-config-custom.xml ###
```
<alfresco-config>
    <config evaluator="string-compare" condition="SAMLFilter">
        <idp>
            <entityId>https://accounts.google.com/o/saml2?idpid=C03q0b20j</entityId>
            <sso>https://accounts.google.com/o/saml2/idp?idpid=C03q0b20j</sso>
            <metadata>/GoogleIDPMetadata-leknarm.com.xml</metadata>
        </idp>
        <sp>
            <goto>gotoURL</goto>
            <entityId>home.leknarm.com</entityId>
            <acs>https://home.leknarm.com:8443/share/acs</acs>
        </sp>
    </config>
</alfresco-config>
```