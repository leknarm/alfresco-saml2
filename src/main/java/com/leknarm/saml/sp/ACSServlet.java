package com.leknarm.saml.sp;

import com.google.gson.Gson;
import com.leknarm.saml.AttributeSet;
import com.leknarm.saml.SAMLUser;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.xml.BasicParserPool;
import net.shibboleth.utilities.java.support.xml.XMLParserException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.core.xml.util.XMLObjectSupport;
import org.opensaml.saml.common.SAMLException;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.*;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml.saml2.metadata.KeyDescriptor;
import org.opensaml.security.credential.BasicCredential;
import org.opensaml.security.credential.UsageType;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.X509Certificate;
import org.opensaml.xmlsec.signature.X509Data;
import org.opensaml.xmlsec.signature.support.SignatureException;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.extensions.config.Config;
import org.springframework.extensions.config.ConfigElement;
import org.springframework.extensions.config.ConfigService;
import org.springframework.extensions.surf.UserFactory;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.ValidationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ACSServlet extends HttpServlet {

    private static final String REPOSITORY_PROTOCOL = "repository.protocol";
    private static final String REPOSITORY_HOST = "repository.host";
    private static final String REPOSITORY_PORT = "repository.port";
    private static final String REPOSITORY_API = "repository.api";
    private static final String REPOSITORY_ADMIN_USER = "repository.admin";
    private static final String REPOSITORY_ADMIN_PASSWORD = "repository.password";
    private static final String REPOSITORY_API_PEOPLE = "people";
    private static final String REPOSITORY_API_LOGIN = "login";
    private static final String REPOSITORY_API_CHANGE_PASSWORD = "person/changepassword/%s";
    private static final String DEFAULT_TICKET_NAME = "alf_ticket";
    private static final String REPOSITORY_USER_PASSWORD = "repository.user-password";

    private static final String IDP_METADATA = "idp.metadata";
    private static final String SP_ACS = "sp.acs";
    private static final String SP_STARTPAGE = "sp.startpage";

    private static Logger logger = LoggerFactory.getLogger(ACSServlet.class);
    private BasicParserPool parsers;
    private BasicCredential cred;
    private final int slack = 300;

    private static final Gson gson = new Gson();
    private static final HttpClient httpClient = new HttpClient();

    @Override
    public void init() throws ServletException {
        parsers = new BasicParserPool();
        parsers.setNamespaceAware(true);
        try {
            parsers.initialize();
        } catch (ComponentInitializationException e) {
            throw new ServletException("Failed to initialize BasicParserPool", e);
        }

        Certificate cert = null;
        EntityDescriptor edesc = null;

        try {
            edesc = (EntityDescriptor) XMLObjectSupport.unmarshallFromInputStream(parsers, Files.newInputStream(Paths.get(getConfigurationValue(IDP_METADATA))));
        } catch (XMLParserException | UnmarshallingException | IOException e) {
            throw new ServletException("Failed to load IDP metadata", e);
        }

        find_cert_loop:
        for (KeyDescriptor kdesc : edesc.getIDPSSODescriptor(SAMLConstants.SAML20P_NS).getKeyDescriptors()) {
            if (kdesc.getUse() != UsageType.SIGNING)
                continue;

            KeyInfo ki = kdesc.getKeyInfo();
            if (ki == null)
                continue;

            for (X509Data x509data : ki.getX509Datas()) {
                for (X509Certificate xcert : x509data.getX509Certificates()) {
                    try {
                        cert = certFromString(xcert.getValue());
                        break find_cert_loop;
                    } catch (CertificateException e) {
                        // keep trying certs; if we don't have one we'll
                        // throw a SAMLException at the end.
                    }
                }
            }
        }
        if (cert == null) {
            logger.error("No valid signing cert found");
        }

        this.cred = new BasicCredential(cert.getPublicKey());
    }

    private static Certificate certFromString(String b64data) throws CertificateException {
        byte[] decoded = DatatypeConverter.parseBase64Binary(b64data);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        return cf.generateCertificate(new ByteArrayInputStream(decoded));
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        logger.info("SAMLResponse: " + req.getParameter("SAMLResponse"));
        AttributeSet attributeSet;

        try {
           attributeSet = validateResponse(req.getParameter("SAMLResponse"));
        } catch (SAMLException e) {
            throw new ServletException("Error when validate response", e);
        }

        String userName = attributeSet.getAttributes().get("email").get(0);
        String ticket = this.getAdministratorTicket();

        if (userName != null) {
            if (!userExists(userName, ticket)) {
                logger.info("Create User with userName: " + userName);
                if (!createUser(attributeSet, ticket)) {
                    throw new ServletException("Error when create repository user.");
                }
            } else {
                logger.info("Change password for userName: " + userName);
                if (!changePassword(userName, ticket)) {
                    throw new ServletException("Error when change repository user password.");
                }
            }

            logger.info("Authenticate with userName: " + userName);
            UserFactory userFactory = (UserFactory) getApplicationContext().getBean("user.factory");
            boolean authenticated = userFactory.authenticate(req, userName, getConfigurationValue(REPOSITORY_USER_PASSWORD));
            if (authenticated) {
                AuthenticationUtil.login(req, resp, userName);
            }
        }

        redirectToGotoURL(req, resp);
    }

    public AttributeSet validateResponse(String authnResponse) throws SAMLException {
        byte[] decoed = DatatypeConverter.parseBase64Binary(authnResponse);
        try {
            authnResponse = new String(decoed, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new SAMLException("Failed to convert byte array to string with UTF-8 encoding", e);
        }

        Response response = parseResponse(authnResponse);
        try {
            validate(response);
        } catch (ValidationException e) {
            throw new SAMLException("Failed to parse response", e);
        }

        // we only look at first assertion
        if (response.getAssertions().size() != 1) {
            throw new SAMLException("Response should have a single assertion.");
        }
        Assertion assertion = response.getAssertions().get(0);

        Subject subject = assertion.getSubject();
        if (subject == null) {
            throw new SAMLException("No subject contained in the assertion.");
        }
        if (subject.getNameID() == null) {
            throw new SAMLException("No NameID found in the subject.");
        }

        String nameId = subject.getNameID().getValue();
        HashMap<String, List<String>> attributes = new HashMap<>();

        for (AttributeStatement atbs : assertion.getAttributeStatements()) {
            for (Attribute atb: atbs.getAttributes()) {
                String name = atb.getName();
                List<String> values = atb.getAttributeValues().stream().map(obj -> obj.getDOM().getTextContent()).collect(Collectors.toList());
                attributes.put(name, values);
            }
        }
        return new AttributeSet(nameId, attributes);
    }

    private Response parseResponse(String authnResponse) throws SAMLException {
        try {
            XMLObject obj = XMLObjectSupport.unmarshallFromReader(parsers, new StringReader(authnResponse));
            return (Response) obj;
        }
        catch (XMLParserException e) {
            throw new SAMLException(e);
        }
        catch (UnmarshallingException e) {
            throw new SAMLException(e);
        }
    }

    private void validate(Response response) throws ValidationException {
        Signature sig = response.getSignature();
        if (sig != null) {
            try {
                SignatureValidator.validate(sig, cred);
            } catch (SignatureException e) {
                throw new ValidationException("Signature validation failed", e);
            }
        }

        if (response.getStatus() == null || response.getStatus().getStatusCode() == null || !(StatusCode.SUCCESS.equals(response.getStatus().getStatusCode().getValue()))) {
            throw new ValidationException("Response has an unsuccessful status code");
        }

        if (!getConfigurationValue(SP_ACS).equals(response.getDestination())) {
            throw new ValidationException("Response is destined for a different endpoint");
        }

        DateTime now = DateTime.now();
        DateTime issueInstant = response.getIssueInstant();

        if (issueInstant != null) {
            if (issueInstant.isBefore(now.minusDays(1).minusSeconds(slack))) {
                throw new ValidationException("Response IssueInstant is in the past");
            }

            if (issueInstant.isAfter(now.plusDays(1).plusSeconds(slack))) {
                throw new ValidationException("Response IssueInstant is in the future");
            }
        }
    }

    private void redirectToGotoURL(HttpServletRequest req, HttpServletResponse resp) {
        String gotoURL = (String)req.getSession().getAttribute("gotoURL");
        if (gotoURL == null) {
            gotoURL = getConfigurationValue(SP_STARTPAGE);
        }
        logger.info("Redirecting to requested URL: " + gotoURL);
        try {
            resp.sendRedirect(gotoURL);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ApplicationContext getApplicationContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(super.getServletContext());
    }

    private String getConfigurationValue(String name) {
        ConfigService configService = (ConfigService) this.getApplicationContext().getBean("web.config");
        Config oauthConfig = configService.getConfig("SAMLFilter");
        if (oauthConfig == null) {
            throw new IllegalStateException("SAML Filter has no configuration");
        }

        String[] parts = StringUtils.split(name, '.');
        ConfigElement mainConfig = oauthConfig.getConfigElement(parts[0]);
        if (mainConfig == null) {
            return null;
        }

        ConfigElement subConfig = mainConfig.getChild(parts[1]);
        if (subConfig == null) {
            return null;
        }

        return subConfig.getValue();
    }

    private String getRepositoryUri(String service) {
        return getConfigurationValue(REPOSITORY_PROTOCOL) + "://" + getConfigurationValue(REPOSITORY_HOST) + ":" + getConfigurationValue(REPOSITORY_PORT) + getConfigurationValue(REPOSITORY_API) + "/" + service;
    }

    protected boolean createUser(AttributeSet attributeSet, String ticket) throws IOException {
        SAMLUser person = new SAMLUser();
        person.setUserName(attributeSet.getAttributes().get("email").get(0));
        person.setPassword(getConfigurationValue(REPOSITORY_USER_PASSWORD));
        person.setEmail(attributeSet.getAttributes().get("email").get(0));
        if (attributeSet.getAttributes().get("firstname").size() > 0) person.setFirstName(attributeSet.getAttributes().get("firstname").get(0));
        if (attributeSet.getAttributes().get("lastname").size() > 0) person.setLastName(attributeSet.getAttributes().get("lastname").get(0));
        if (attributeSet.getAttributes().get("jobtitle").size() > 0) person.setJobTitle(attributeSet.getAttributes().get("jobtitle").get(0));

        String json = gson.toJson(person);
        logger.debug("json look like: " + json);

        EntityEnclosingMethod postPeopleMethod = new PostMethod(getRepositoryUri(REPOSITORY_API_PEOPLE));
        this.addTicketParameter(postPeopleMethod, ticket);

        postPeopleMethod.setRequestEntity(new StringRequestEntity(json, "application/json", "utf-8"));
        if (new HttpClient().executeMethod(postPeopleMethod) == HttpStatus.SC_OK) {
            return true;
        }
        return false;
    }

    protected boolean changePassword(String userName, String ticket) throws IOException {
        EntityEnclosingMethod postPeopleMethod = new PostMethod(getRepositoryUri(String.format(REPOSITORY_API_CHANGE_PASSWORD, userName)));
        this.addTicketParameter(postPeopleMethod, ticket);

        Map<String, String> pw = new HashMap<>();
        pw.put("newpw", getConfigurationValue(REPOSITORY_USER_PASSWORD));
        String json = gson.toJson(pw);
        postPeopleMethod.setRequestEntity(new StringRequestEntity(json, "application/json", "utf-8"));
        if (new HttpClient().executeMethod(postPeopleMethod) == HttpStatus.SC_OK) {
            return true;
        }
        return false;
    }

    protected boolean userExists(String userName, String ticket) throws IOException {
        GetMethod get = new GetMethod((getRepositoryUri(REPOSITORY_API_PEOPLE) + "/" + userName));

        this.addTicketParameter(get, ticket);
        return httpClient.executeMethod(get) == HttpStatus.SC_OK;
    }

    protected void addTicketParameter(HttpMethodBase method, String ticket) {
        method.setPath(method.getPath() + "?" + DEFAULT_TICKET_NAME + "=" + ticket);
    }

    protected String getAdministratorTicket() throws IOException {
        GetMethod getTicketMethod = new GetMethod(getRepositoryUri(REPOSITORY_API_LOGIN));
        getTicketMethod.setQueryString(new NameValuePair[]{
                new NameValuePair("u", getConfigurationValue(REPOSITORY_ADMIN_USER)),
                new NameValuePair("pw", getConfigurationValue(REPOSITORY_ADMIN_PASSWORD))
        });

        if (httpClient.executeMethod(getTicketMethod) == HttpStatus.SC_OK) {
            // return only ticket string
            return getTicketMethod.getResponseBodyAsString().substring(47, 94);
        } else {
            return null;
        }
    }

}