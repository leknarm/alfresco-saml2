package com.leknarm.saml.sp;

import com.leknarm.saml.OpenSAMLUtils;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.opensaml.core.config.InitializationException;
import org.opensaml.core.config.InitializationService;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.encoder.MessageEncodingException;
import org.opensaml.saml.common.messaging.context.SAMLEndpointContext;
import org.opensaml.saml.common.messaging.context.SAMLPeerEntityContext;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.binding.encoding.impl.HTTPRedirectDeflateEncoder;
import org.opensaml.saml.saml2.core.*;
import org.opensaml.saml.saml2.metadata.Endpoint;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.xmlsec.SignatureSigningParameters;
import org.opensaml.xmlsec.config.JavaCryptoValidationInitializer;
import org.opensaml.xmlsec.context.SecurityParametersContext;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.extensions.config.Config;
import org.springframework.extensions.config.ConfigElement;
import org.springframework.extensions.config.ConfigService;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Provider;
import java.security.Security;

public class SAMLAuthenticationFilter implements Filter {

    private static Logger logger = LoggerFactory.getLogger(SAMLAuthenticationFilter.class);

    private static final String IDP_SSO_SERVICE = "idp.sso";

    private static final String SP_ENTITY_ID = "sp.entityId";
    private static final String SP_ACS = "sp.acs";

    private ServletContext servletContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.servletContext = filterConfig.getServletContext();

        JavaCryptoValidationInitializer javaCryptoValidationInitializer = new JavaCryptoValidationInitializer();
        try {
            javaCryptoValidationInitializer.init();
        } catch (InitializationException e) {
            e.printStackTrace();
        }

        for (Provider jceProvider : Security.getProviders()) {
            logger.info(jceProvider.getInfo());
        }

        try {
            logger.info("Initializing");
            InitializationService.initialize();
        } catch (InitializationException e) {
            throw new RuntimeException("Initialization failed");
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (AuthenticationUtil.isAuthenticated(request)) {
            logger.debug("already authenticated");
            filterChain.doFilter(request, response);
            return;
        }

        setGotoURLOnSession(request);
        redirectUserForAuthentication(response);
    }

    private void setGotoURLOnSession(HttpServletRequest request) {
        request.getSession().setAttribute("gotoURL", request.getRequestURL().toString());
    }

    private void redirectUserForAuthentication(HttpServletResponse httpServletResponse) {
        AuthnRequest authnRequest = buildAuthnRequest();
        redirectUserWithRequest(httpServletResponse, authnRequest);
    }

    private AuthnRequest buildAuthnRequest() {
        AuthnRequest authnRequest = OpenSAMLUtils.buildSAMLObject(AuthnRequest.class);
        authnRequest.setIssueInstant(new DateTime());
        authnRequest.setDestination(getIPDSSODestination());
        authnRequest.setProtocolBinding(SAMLConstants.SAML2_ARTIFACT_BINDING_URI);
        authnRequest.setAssertionConsumerServiceURL(getAssertionConsumerEndpoint());
        authnRequest.setID(OpenSAMLUtils.generateSecureRandomId());
        authnRequest.setIssuer(buildIssuer());
        authnRequest.setNameIDPolicy(buildNameIdPolicy());
        authnRequest.setRequestedAuthnContext(buildRequestedAuthnContext());

        return authnRequest;
    }

    private Issuer buildIssuer() {
        Issuer issuer = OpenSAMLUtils.buildSAMLObject(Issuer.class);
        issuer.setValue(getSPIssuerValue());

        return issuer;
    }

    private NameIDPolicy buildNameIdPolicy() {
        NameIDPolicy nameIDPolicy = OpenSAMLUtils.buildSAMLObject(NameIDPolicy.class);
        nameIDPolicy.setAllowCreate(true);

        nameIDPolicy.setFormat(NameIDType.TRANSIENT);

        return nameIDPolicy;
    }

    private RequestedAuthnContext buildRequestedAuthnContext() {
        RequestedAuthnContext requestedAuthnContext = OpenSAMLUtils.buildSAMLObject(RequestedAuthnContext.class);
        requestedAuthnContext.setComparison(AuthnContextComparisonTypeEnumeration.BETTER);

        AuthnContextClassRef passwordAuthnContextClassRef = OpenSAMLUtils.buildSAMLObject(AuthnContextClassRef.class);
        passwordAuthnContextClassRef.setAuthnContextClassRef(AuthnContext.PASSWORD_AUTHN_CTX);

        requestedAuthnContext.getAuthnContextClassRefs().add(passwordAuthnContextClassRef);

        return requestedAuthnContext;
    }

    private void redirectUserWithRequest(HttpServletResponse httpServletResponse, AuthnRequest authnRequest) {

        MessageContext context = new MessageContext();

        context.setMessage(authnRequest);

        SAMLPeerEntityContext peerEntityContext = context.getSubcontext(SAMLPeerEntityContext.class, true);

        SAMLEndpointContext endpointContext = peerEntityContext.getSubcontext(SAMLEndpointContext.class, true);
        endpointContext.setEndpoint(getIPDEndpoint());

        SignatureSigningParameters signatureSigningParameters = new SignatureSigningParameters();
        signatureSigningParameters.setSigningCredential(SPCredentials.getCredential());
        signatureSigningParameters.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);


        context.getSubcontext(SecurityParametersContext.class, true).setSignatureSigningParameters(signatureSigningParameters);

        HTTPRedirectDeflateEncoder encoder = new HTTPRedirectDeflateEncoder();

        encoder.setMessageContext(context);
        encoder.setHttpServletResponse(httpServletResponse);

        try {
            encoder.initialize();
        } catch (ComponentInitializationException e) {
            throw new RuntimeException(e);
        }

        logger.info("AuthnRequest: ");
        OpenSAMLUtils.logSAMLObject(authnRequest);

        logger.info("Redirecting to IDP");
        try {
            encoder.encode();
        } catch (MessageEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getIPDSSODestination() {
        return getConfigurationValue(IDP_SSO_SERVICE);
    }

    private String getAssertionConsumerEndpoint() {
        return getConfigurationValue(SP_ACS);
    }

    private String getSPIssuerValue() {
        return getConfigurationValue(SP_ENTITY_ID);
    }

    private Endpoint getIPDEndpoint() {
        SingleSignOnService endpoint = OpenSAMLUtils.buildSAMLObject(SingleSignOnService.class);
        endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
        endpoint.setLocation(getIPDSSODestination());

        return endpoint;
    }

    @Override
    public void destroy() {

    }

    private ApplicationContext getApplicationContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    private String getConfigurationValue(String name) {
        ConfigService configService = (ConfigService) this.getApplicationContext().getBean("web.config");
        Config oauthConfig = configService.getConfig("SAMLFilter");
        if (oauthConfig == null) {
            throw new IllegalStateException("SAML Filter has no configuration");
        }

        String[] parts = StringUtils.split(name, '.');
        ConfigElement mainConfig = oauthConfig.getConfigElement(parts[0]);
        if (mainConfig == null) {
            return null;
        }

        ConfigElement subConfig = mainConfig.getChild(parts[1]);
        if (subConfig == null) {
            return null;
        }

        return subConfig.getValue();
    }

//    protected String createUser(Person gplusPerson, String ticket) throws IOException {
//        GooglePlusPerson person = new GooglePlusPerson();
//        person.setUserName(gplusPerson.getEmails().get(0).getValue());
//        person.setPassword(getConfigurationValue(USER_PASSWORD));
//        person.setEmail(gplusPerson.getEmails().get(0).getValue());
//        person.setFirstName(gplusPerson.getName().getGivenName());
//        person.setLastName(gplusPerson.getName().getFamilyName());
//        person.setJobTitle(gplusPerson.getOccupation());
//        if (gplusPerson.getOrganizations() != null) {
//            person.setOrganization(gplusPerson.getOrganizations().get(0).getName());
//        }
//
//        String json = gson.toJson(person);
//        logger.debug("json look like: " + json);
//
//        EntityEnclosingMethod postPeopleMethod = new PostMethod(getRepositoryUri(REPOSITORY_API_PEOPLE));
//        this.addTicketParameter(postPeopleMethod, ticket);
//
//        postPeopleMethod.setRequestEntity(new StringRequestEntity(json, "application/json", "utf-8"));
//        if (new HttpClient().executeMethod(postPeopleMethod) == HttpStatus.SC_OK) {
//            return person.getUserName();
//        } else {
//            return null;
//        }
//    }
//
//    protected boolean userExists(String username, String ticket) throws IOException {
//        GetMethod get = new GetMethod((getRepositoryUri(REPOSITORY_API_PEOPLE) + "/" + username));
//
//        this.addTicketParameter(get, ticket);
//        return httpClient.executeMethod(get) == HttpStatus.SC_OK;
//    }
//
//    protected String getAdministratorTicket() throws IOException {
//        GetMethod getTicketMethod = new GetMethod(getRepositoryUri(REPOSITORY_API_LOGIN));
//        getTicketMethod.setQueryString(new NameValuePair[]{
//                new NameValuePair("u", getConfigurationValue(REPOSITORY_ADMIN_USER)),
//                new NameValuePair("pw", getConfigurationValue(REPOSITORY_ADMIN_PASSWORD))
//        });
//
//        if (httpClient.executeMethod(getTicketMethod) == HttpStatus.SC_OK) {
//            // return only ticket string
//            return getTicketMethod.getResponseBodyAsString().substring(47, 94);
//        } else {
//            return null;
//        }
//    }
//
//    protected void addTicketParameter(HttpMethodBase method, String ticket) {
//        method.setPath(method.getPath() + "?" + DEFAULT_TICKET_NAME + "=" + ticket);
//    }

}
