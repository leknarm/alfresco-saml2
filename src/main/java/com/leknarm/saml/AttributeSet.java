package com.leknarm.saml;

import java.util.List;
import java.util.Map;

public class AttributeSet {
    private String nameId;
    private Map<String, List<String>> attributes;

    public AttributeSet(String nameId,
                        Map<String, List<String>> attributes)
    {
        this.nameId = nameId;
        this.attributes = attributes;
    }

    public String getNameId()
    {
        return nameId;
    }

    public Map<String, List<String>> getAttributes()
    {
        return this.attributes;
    }
}
